---
title: "Survey of Advanced Language Features | Lesson 36"
type: "docs"
date: 2020-01-05T15:34:00-07:00
draft: true
---


For macro inclusion, see: https://code.thheller.com/blog/shadow-cljs/2019/10/12/clojurescript-macros.html
For implicit macro loading, see: https://gist.github.com/mfikes/40b634b1ef8e317d8d5a